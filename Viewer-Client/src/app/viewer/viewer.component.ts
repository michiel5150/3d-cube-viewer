import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { RotationDataService } from './rotation-data.service';
import * as THREE from 'three';
import { Rotation } from './rotation';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {
  @ViewChild('rendererContainer', { static: true }) rendererContainer: ElementRef;
  @HostListener('window:resize', ['$event'])
  onWindowResize(event) {
    this.renderer.setSize(event.target.innerWidth, event.target.innerHeight)
  }

  title = 'RotatingCube';
  renderer = new THREE.WebGLRenderer();
  scene = null;
  camera = null;
  mesh = null;
  rotation: Rotation = new Rotation(0, 0, 0);
  animationType : number = 1;


  ngOnInit() {
    var serverWebSocketAddress = 'ws://' + environment.serverIp + ':' + environment.serverWebSocketPort + '/';

    this.rotationDataService.Connect(serverWebSocketAddress).subscribe(message => {
      if ((String)(message.data).substring(0, 3) == "{\"x") {
        this.rotation = JSON.parse(message.data);
      }
    });
  }

  constructor(private rotationDataService: RotationDataService) {
    this.scene = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);

    var geometry = new THREE.BoxGeometry(300, 300, 300, 10, 10, 10);
    //var material = new THREE.MeshMatcapMaterial({color: 0xffffff, wireframe: false});
    var material = new THREE.MeshNormalMaterial();
    this.mesh = new THREE.Mesh(geometry, material);

    this.scene.add(this.mesh);

    this.camera.position.z = 1000;
  }

  ngAfterViewInit() {
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.rendererContainer.nativeElement.appendChild(this.renderer.domElement);
    this.animate();
  }

  animate() {
    window.requestAnimationFrame(() => this.animate());

    // using roll pitch yaw
    //this.animateUsingRollPitchYaw();

    // using quaterion to prevent gimble lock
    this.animateUsingQuaternion();

    this.renderer.render(this.scene, this.camera);
  }

  animateUsingRollPitchYaw() {
    this.mesh.rotation.x = this.rotation.xAxis;
    this.mesh.rotation.y = this.rotation.yAxis;
    this.mesh.rotation.z = this.rotation.zAxis;
  }

  animateUsingQuaternion() {
    // Declare X and Y axes
    var axisX = new THREE.Vector3(1, 0, 0);
    var axisY = new THREE.Vector3(0, 1, 0);
    var axisZ = new THREE.Vector3(0, 0, 1);

    // Init quaternions that will rotate along each axis
    var quatX = new THREE.Quaternion();
    var quatY = new THREE.Quaternion();
    var quatZ = new THREE.Quaternion();

    // Set quaternions from each axis (in radians)...
    quatX.setFromAxisAngle(axisX, this.rotation.xAxis);
    quatY.setFromAxisAngle(axisY, this.rotation.yAxis);
    quatZ.setFromAxisAngle(axisZ, this.rotation.zAxis);

    // ...then multiply them to get final rotation
    quatY.multiply(quatX);
    quatZ.multiply(quatY);

    // Apply multiplied rotation to your mesh
    this.mesh.quaternion.copy(quatZ);
  }

}
